import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class GomokuScreen extends Screen{
		

	public GomokuScreen(){
		
	    frame = new JFrame("Gomoku");
	    frame.setSize(800, 565);
	    frame.setLocation(180, 200);

	    board = new GomokuChessBoard();
	    this.ruleOfMoving = new GomokuRuleOfMoving((GomokuChessBoard) board);
	    this.ruleOfVictory = new GomokuRuleOfVictory((GomokuChessBoard) board);
	    
	    board.getBoard().setSize(535, 555);

	    this.connect();
	    this.setMessagePanel();
    
	    frame.setLayout(new BorderLayout());
	    frame.add(board.getBoard(),BorderLayout.WEST);
	    frame.add(messagePanel,BorderLayout.EAST);
	    frame.setVisible(true);
	    frame.setResizable(false);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	protected void setMessagePanel() {
		this.messagePanel = new JPanel();
		this.messagePanel.setSize(265, 560);
		this.messagePanel.setLayout(new GridLayout(2,1));
		this.messagePanel.add(board.getDisplayPlayerTurn());
		this.messagePanel.add(board.getDisplayWinner());
		
	}

	public static void main(String[] args) {
		GomokuScreen  GS = new GomokuScreen();

	}

}
