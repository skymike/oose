import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public abstract class RuleOfMoving implements MouseListener{
	
	protected int playerFlag;
	protected final int UNDEFINE= 0;
	protected final int PLAYER1 = 1;
	protected final int PLAYER2 = 2;
	

	public int getPlayerFlag() {
		return playerFlag;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	protected void changePlayerFlag(){
		if(playerFlag == PLAYER1){
			playerFlag = PLAYER2;
		}else if(playerFlag == PLAYER2){
			playerFlag = PLAYER1;
		}
	}
}
