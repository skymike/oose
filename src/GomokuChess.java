import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class GomokuChess extends Chess{

	private ImageIcon pieceImage;

	public GomokuChess(int X ,int Y){
		super.Xposition = X;
		super.Yposition = Y;

	}

	public ImageIcon getPieceImage() {
		return pieceImage;
	}

	public void setPieceImage(ImageIcon pieceImage) {
		this.pieceImage = pieceImage;
		this.setIcon(pieceImage);
	}
	
}
