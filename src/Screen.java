import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public abstract class Screen {

	protected JFrame frame;
	protected JPanel messagePanel;
	protected ChessBoard board;
	protected RuleOfMoving ruleOfMoving;
	protected RuleOfVictory ruleOfVictory;
	
	protected void connect(){
		
	    board.setRuleOfMoving(ruleOfMoving);
	    board.setRuleOfVictory(ruleOfVictory);
	    board.newBoard();
	}
	
	protected abstract void setMessagePanel();
	
}
