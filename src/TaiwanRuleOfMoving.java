import java.awt.event.MouseEvent;


public class TaiwanRuleOfMoving extends RuleOfMoving{
	
	private final int COMMANDER = 7;
	private final int KNIGHT = 6;
	private final int DEPUTY = 5;
	private final int CHARIOT = 4;
	private final int HORSE= 3;
	private final int GUN = 2;
	private final int SOLDIER = 1;
	private final int NONEXISTENCE = 0;


	

	private boolean secondMovingFlag;
	private boolean completeMovingFlag;
	private TaiwanChessBoard board;
	private TaiwanChess firstClickChess;
	private String colorOfPlayer1;
	private String colorOfPlayer2;
	private String colorOfCurrentPlayer;
	private TaiwanChess piece;
	private int[][] chessOnBoard;
	
	public TaiwanRuleOfMoving(TaiwanChessBoard board) {
		
		this.board = board;
		this.chessOnBoard = board.getChessOnBoard();
		super.playerFlag = UNDEFINE;
	}
	

	public String getColorOfPlayer1() {
		return colorOfPlayer1;
	}
	
	

	public String getColorOfCurrentPlayer() {
		return colorOfCurrentPlayer;
	}

	public TaiwanChess getFirstClickChess() {
		return firstClickChess;
	}


	public void setFirstClickChess(TaiwanChess firstClickChess) {
		this.firstClickChess = firstClickChess;
	}

	private void setColorOfPlayer(){
		
		if(this.piece.getColor() == "Black"){
			colorOfPlayer1 = "Black";
		}else{
			colorOfPlayer2 = "Red";
		}
		if(this.piece.getColor() == "Red"){
			colorOfPlayer1 = "Red";
		}else{
			colorOfPlayer2 = "Black";
		}
	}

	


	@Override
	public void mouseClicked(MouseEvent event) {
		
		if(event.getButton() == MouseEvent.BUTTON1){
			
			piece = (TaiwanChess)event.getComponent();
			System.out.println(playerFlag);
			System.out.println(secondMovingFlag);
			System.out.println(completeMovingFlag);
			System.out.println(colorOfPlayer1);
			if(playerFlag == UNDEFINE){
				colorOfPlayer1 = piece.getColor();
				playerFlag = PLAYER1;
			}
			if(secondMovingFlag == false){
				this.determinefirstClick();

			}else{
				
				this.determineSecondClick();
			}
			if(completeMovingFlag == true){
				this.changePlayerFlag();
				this.updateFirstClickChess(null);
				completeMovingFlag = false;
				secondMovingFlag = false;
			}
	
		}else if(event.getButton() == MouseEvent.BUTTON3){
			
			this.updateFirstClickChess(null);
			completeMovingFlag = false;
			secondMovingFlag = false;
		}
		
	}
	private boolean isPlayerMatchColor(){

		if(this.colorOfCurrentPlayer.equals(piece.getColor())){
			return true;
		}else{
			return false;
		}

	}
	private boolean isOpponentMatchColor(){
		if(this.isPlayerMatchColor()){
			return false;
		}else{
			return true;
		}
	}
	private boolean isLegalMoving(){
		int XBefore,XAfter,YBefore,YAfter;
		XBefore = firstClickChess.getXposition();
		XAfter = piece.getXposition();
		YBefore = firstClickChess.getYposition();
		YAfter = piece.getYposition();
		if((Math.abs(XBefore-XAfter) <= 1) && (Math.abs(YBefore-YAfter) <= 1)){
			if((Math.abs(XBefore-XAfter) == 1) && (Math.abs(YBefore-YAfter) == 1)){
				return false;
			}else if((Math.abs(XBefore-XAfter) == 0) && (Math.abs(YBefore-YAfter) == 0)){
				return false;
			}else{
				return true;
			}
		}
		return false;
	}
	private void moveChess(){
		piece.setExistenceFlag(true);
		piece.setColor(firstClickChess.getColor());
		piece.setRank(firstClickChess.getRank());
		piece.setFrontSide(firstClickChess.getFrontSide());
		piece.openChess();
		firstClickChess.removeChess();	
		chessOnBoard[piece.getXposition()][piece.getYposition()] = chessOnBoard[firstClickChess.getXposition()][firstClickChess.getYposition()];
		chessOnBoard[firstClickChess.getXposition()][firstClickChess.getYposition()] = NONEXISTENCE;
	}

	private void determinefirstClick(){
		if(piece.isExistenceFlag()){
			if(piece.isOpenFlag()){
				if(isPlayerMatchColor()){
					this.updateFirstClickChess(piece);
					secondMovingFlag = true;
				}else{
					secondMovingFlag = false;
				}
			}else{
				piece.openChess();
				secondMovingFlag = false;
				completeMovingFlag = true;
			}
		}else{
			secondMovingFlag = false;
		}
		//System.out.println(secondMovingFlag);
		//System.out.println(completeMovingFlag);

	}
	private void determineSecondClick(){
		if(piece.isExistenceFlag()){
			if(piece.isOpenFlag()){
				if(this.isOpponentMatchColor()){
					if(firstClickChess.getRank() == GUN){
						if(this.isLegalGunCapturing()){
							this.moveChess();
							completeMovingFlag = true;
						}else{
							completeMovingFlag = false;
						}
					}else{
						if(this.isLegalCaptureChess()){
							this.moveChess();
							completeMovingFlag = true;
						}else{
							completeMovingFlag = false;
						}
					}
				}else{
					completeMovingFlag = false;
				}
			}else{
				completeMovingFlag = false;
			}
		}else{
			if(this.isLegalMoving()){
				this.moveChess();
				completeMovingFlag = true;
			}else{
				completeMovingFlag = false;
			}
		}
	}
	private boolean isLegalGunCapturing(){
		int XBefore,XAfter,YBefore,YAfter,betweenNum = 0;
		XBefore = firstClickChess.getXposition();
		XAfter = piece.getXposition();
		YBefore = firstClickChess.getYposition();
		YAfter = piece.getYposition();
		
		if((XBefore == XAfter) || (YBefore == YAfter)){
			if((XBefore == XAfter) && (YBefore == YAfter)){
				return false;
			}else if(XBefore == XAfter){
				if(YBefore > YAfter){
					while(YBefore-1 > YAfter){
						if(chessOnBoard[XBefore][YBefore-1] > 0){
							betweenNum = betweenNum + 1;
						}
						YBefore = YBefore - 1;
					}
				}else{
					while(YBefore+1 < YAfter){
						if(chessOnBoard[XBefore][YBefore+1] > 0){
							betweenNum = betweenNum + 1;
						}
						YBefore = YBefore + 1;
					}
				}
			}else{
				if(XBefore > XAfter){
					while(XBefore-1 > XAfter){
						if(chessOnBoard[XBefore-1][YBefore] > 0){
							betweenNum = betweenNum + 1;
						}
						XBefore = XBefore - 1;
					}
				}else{
					while(XBefore+1 < XAfter){
						if(chessOnBoard[XBefore+1][YBefore] > 0){
							betweenNum = betweenNum + 1;
						}
						XBefore = XBefore + 1;
					}
				}
			}
			if(betweenNum == 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	private boolean isLegalCaptureChess(){
		if(this.isLegalMoving()){
			if(firstClickChess.getRank() >= piece.getRank()){
				if(firstClickChess.getRank() == COMMANDER && piece.getRank() == SOLDIER){
					return false;
				}else{
					return true;
				}
			}else if(firstClickChess.getRank() == SOLDIER && piece.getRank() == COMMANDER){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	@Override
	protected void changePlayerFlag(){
		super.changePlayerFlag();

		this.changePlayerColor();
		board.updateDisplayPlayerTurn();
	}
	private void changePlayerColor(){
		if(this.colorOfCurrentPlayer == "Black"){
			colorOfCurrentPlayer = "Red";
		}else{
			colorOfCurrentPlayer = "Black";
		}
	}
	private void updateFirstClickChess(TaiwanChess c){
		this.firstClickChess = c;
		board.updateDisplayPlayerHoldChess();
	}
}
