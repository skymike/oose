import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class TaiwanScreen extends Screen{
	
	public TaiwanScreen(){
		
	    frame = new JFrame("Taiwan");
	    frame.setSize(815, 600);
	    frame.setLocation(200, 100);

	    board = new TaiwanChessBoard();
	    super.ruleOfMoving = new TaiwanRuleOfMoving((TaiwanChessBoard) board);
	    super.ruleOfVictory = new TaiwanRuleOfVictory((TaiwanChessBoard) board);
	    
	    board.getBoard().setSize(800, 400);

	    this.connect();
	    this.setMessagePanel();
    
	    frame.setLayout(new BorderLayout());
	    frame.add(board.getBoard(),BorderLayout.NORTH);
	    frame.add(messagePanel,BorderLayout.SOUTH);
	    frame.setVisible(true);
	    frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}

	@Override
	protected void setMessagePanel() {
		TaiwanChessBoard board = (TaiwanChessBoard)this.board;
		this.messagePanel = new JPanel();
		this.messagePanel.setSize(800, 200);
		this.messagePanel.setLayout(new GridLayout(0,1));
		this.messagePanel.add(board.getDisplayWinner());
		this.messagePanel.add(board.getDisplayPlayerTurn());
		this.messagePanel.add(board.getDisplayPlayerHoldChess());
		
		
	}
	
	public static void main(String[] args) {
		TaiwanScreen  GS = new TaiwanScreen();

	}

}
